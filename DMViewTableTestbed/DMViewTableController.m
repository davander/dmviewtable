//
//  DMViewTableController.m
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-08.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMViewTableController.h"

#import "DMViewTable.h"

#define DMVIEWTABLECELL_TAG 31337

@implementation DMViewTableController

@synthesize viewControllers;
@synthesize viewTable;
@synthesize grouped;
@synthesize sectioned;
@synthesize closeHandler;

-(id)init {
    if ((self = [super init])) {
        self.grouped = YES;
        self.sectioned = YES; // defaults.
        
    }
    return self;
}

-(void)dealloc {
    self.viewControllers = nil;
    self.closeHandler = nil;
    [super dealloc];
}

-(void)loadView {
    CGRect frame = [[UIScreen mainScreen] applicationFrame]; // (CGRect){};
    self.view = [[[UIView alloc] initWithFrame:frame] autorelease];
    self.view.backgroundColor  = [UIColor greenColor];
    self.viewTable = [[[DMViewTable alloc] initWithFrame:self.view.bounds style:(self.grouped ? UITableViewStyleGrouped : UITableViewStylePlain)] autorelease];
    self.viewTable.autoresizingMask = UIViewAutoresizingFlexibleWidth + UIViewAutoresizingFlexibleHeight;
    self.viewTable.allowsMultipleSelection = YES;
    [self.view addSubview:self.viewTable];
    self.viewTable.delegate = self;
    self.viewTable.dataSource = self;
}

#pragma mark -
#pragma mark orientation

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark -
#pragma mark Showing and closing

-(void)viewWillDisappear:(BOOL)animated {
    NSUInteger ind = [[self.navigationController viewControllers] indexOfObject:self];
    if (ind == NSNotFound) {
        if (self.closeHandler) self.closeHandler(self);
    }
}


#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return (self.sectioned ? [self.viewControllers count] : 1);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (self.sectioned ? 1 : [self.viewControllers count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ind = (self.sectioned ? indexPath.section : indexPath.row);
    
    static NSString *CellIdentifier = @"DMViewTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    UIView *oldControllerView = [cell viewWithTag:DMVIEWTABLECELL_TAG];
    [oldControllerView removeFromSuperview];
    
    UIViewController <DMViewTableCellController>*controller = [self.viewControllers objectAtIndex:ind];
    NSAssert(![controller isKindOfClass:[UIViewController class]], @"You cannot use subclasses of UIViewController because multiple UIViewControllers on screen at once is mostly unsupported  by iOS. Override this if you want, but believe me they behave weird. Simple solution is to use view controller objects that are not subclasses of UIViewController. See example code for... well, for an example.");
    controller.view.tag = DMVIEWTABLECELL_TAG;
    cell.clipsToBounds = YES; // Just me, or is it weird that this is not on by default?
    cell.contentView.clipsToBounds = YES;
    [cell.contentView addSubview:controller.view];
    controller.view.clipsToBounds = YES;
    return cell;
}

#pragma mark -
#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ind = (self.sectioned ? indexPath.section : indexPath.row);
    UIViewController <DMViewTableCellController>*controller = [self.viewControllers objectAtIndex:ind];
    NSAssert(controller && [controller respondsToSelector:@selector(height)], @"Controller could not be retrieved.");
    return [controller height];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ind = (self.sectioned ? indexPath.section : indexPath.row);
    UIViewController <DMViewTableCellController>*controller = [self.viewControllers objectAtIndex:ind];
    [self.viewTable beginUpdates];
    [controller expandCell:YES];
    [self.viewTable endUpdates];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger ind = (self.sectioned ? indexPath.section : indexPath.row);
    UIViewController <DMViewTableCellController>*controller = [self.viewControllers objectAtIndex:ind];
    
    [self.viewTable beginUpdates];
    [controller collapseCell:YES];
    [self.viewTable endUpdates];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    for (DMViewTableController *viewCell in self.viewControllers) {
        if ([viewCell respondsToSelector:@selector(viewDragged:)]) [viewCell performSelector:@selector(viewDragged:) withObject:scrollView];
    }
}

#pragma mark - Overriders

-(BOOL)hasUnsavedChanges {
    return NO;
}

@end
