//
//  DMImageViewCellController.h
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-14.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMViewCellController.h"

@interface DMImageViewCellController : DMViewCellController

@property (nonatomic, assign) IBOutlet UIImageView *imageView;

@end
