//
//  DMCancelViewCellController.m
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-03-13.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMButtonViewCellController.h"


@implementation DMButtonViewCellController

@synthesize button;
@synthesize buttonHandler;

-(CGFloat)height {
    return 77.0f;
}

-(id)initWithTitle:(NSString *)newTitle {
    if ((self = [super init])) {
      [self.button setTitle:newTitle forState:UIControlStateNormal];
    }
    return self;
}

-(void)dealloc {
    self.buttonHandler = nil;
    [super dealloc];
}

-(IBAction)buttonPress:(id)sender {
    if (self.buttonHandler) self.buttonHandler();
}

-(BOOL)expanded {
    return YES;
}

@end
