Pod::Spec.new do |s|
  s.name         = "DMViewTable"
  s.version      = "0.0.1"
  s.summary      = "A table of independent views."
  s.platform     = :ios, '5.0'
  s.source       = { :git => "https://davander@bitbucket.org/davander/dmviewtable.git", :commit => "4fb92931aa048cdd488eacfcfcf376019e3dc8f8" }
  s.description  = <<-DESC
  views on views on views.
                    DESC
  s.homepage     = "https://bitbucket.org/davander/dmviewtable.git"
  s.license      = 'MIT'
  s.author       = { "Cory Alder" => "cory@davander.com" }
  s.source_files = 'DMViewTableTestbed/DMViewTableController.{h,m}', 'DMViewTableTestbed/DMViewTable.{h,m}', 'DMViewTableTestbed/DM*CellController.{h,m}'
  s.resources = "DMViewTableTestbed/*.xib", "DMActivityInstagram/DMResizerViewController.xib"
  s.requires_arc = false
end
