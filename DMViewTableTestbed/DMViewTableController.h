//
//  DMViewTableController.h
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-08.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMViewTable;

@interface DMViewTableController : UIViewController <UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate> 

@property (nonatomic, retain) NSArray *viewControllers;
@property (nonatomic, assign) DMViewTable *viewTable;
@property (readwrite) BOOL grouped; // tableview is a grouped table view, not a plain one.
@property (readwrite) BOOL sectioned; // each controller gets its own section.

@property (nonatomic, copy) void (^closeHandler) (DMViewTableController *viewTableController);

-(BOOL)hasUnsavedChanges;

@end
