//
//  DMAccountCellController.m
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-14.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMAccountCellController.h"

@implementation DMAccountCellController

@synthesize cells;

@synthesize loginMode;

@synthesize emailField;
@synthesize passwordField;
@synthesize nameField;
@synthesize marketingEmailSwitch;
@synthesize notificationSwitch;
@synthesize submitHandler;
@synthesize validationHandler;

@synthesize delegate;

-(void)dealloc {
    self.delegate = nil;
    
    [super dealloc];
}

-(BOOL)expanded { return YES; }

-(IBAction)toggleLoginOrNew:(id)sender {
    // switch cells to just login / password
    self.loginMode = !self.loginMode;
    [sender setTitle:(self.loginMode ? @"Login" : @"New Account") forState:UIControlStateNormal];
    NSLog(@"Toggle login / new account mode");
    [sender setBackgroundColor:(self.loginMode ? [UIColor orangeColor] : [UIColor blueColor])];
}

-(IBAction)submitLoginOrNew:(id)sender {
    // collect all information
    NSString *name = self.nameField.text;
    NSString *password = self.passwordField.text;
    NSString *email = self.emailField.text;
    BOOL marketing = self.marketingEmailSwitch.isOn;
    BOOL notifications = self.notificationSwitch.isOn;
    
    NSMutableDictionary *dict = [[[NSMutableDictionary alloc] initWithCapacity:10] autorelease];
    [dict setObject:name forKey:@"username"];
    [dict setObject:password forKey:@"password"];
    [dict setObject:email forKey:@"email"];
    [dict setObject:[NSNumber numberWithBool:marketing] forKey:@"marketing"];
    [dict setObject:[NSNumber numberWithBool:notifications] forKey:@"notifications"];
    [dict setObject:[NSNumber numberWithBool:self.loginMode] forKey:@"login_mode"];
    
    // modify current "me" record or create a new one with info
    NSLog(@"Properties dictionary %@",dict);
    
    if (self.validationHandler) {
        NSArray *validationErrors = self.validationHandler(dict);
        if (![validationErrors count]) NSLog(@"Submit successful (no errors in input), so dismiss?");
        else {
            NSLog(@"Errors in submit data, alert the user... %@", validationErrors);
            return;
        }
    }
    
    if (self.submitHandler) self.submitHandler(dict);
}


-(void)viewDragged:(UIScrollView *)view {
    NSArray *subviews = [self.view subviews];
    
    for (UIView *a_view in subviews) {
        [a_view resignFirstResponder];
    }
}


#pragma mark -
#pragma mark server response

/*

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
    NSString *message = @"";
    NSString *title = @"";
    
    NSInteger statusCode = [[objectLoader response] statusCode];
    if (statusCode == 401) {
        message = @"Please re-enter your email and password, and try again";
        title = @"Authentication failed";
    } else if (statusCode == 422) {
        message = [[objectLoader response] bodyAsString];
        title = @"Account creation failed";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [alert autorelease];
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didLoadObject:(id)object {
    NSLog(@"Did load OBJECT## %@", object);
    [delegate dismissModalViewControllerAnimated:YES];
}

- (void)objectLoaderDidLoadUnexpectedResponse:(RKObjectLoader*)objectLoader {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server error" message:@"Encountered an unexpected response from the server..." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
    [alert autorelease];
}

*/

@end
