//
//  DMAccountCellController.h
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-14.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMViewCellController.h"

@interface DMAccountCellController : DMViewCellController

@property (nonatomic, retain) NSArray *cells;

@property (readwrite) BOOL loginMode;

@property (nonatomic, assign) IBOutlet UITextField *emailField;
@property (nonatomic, assign) IBOutlet UITextField *nameField;
@property (nonatomic, assign) IBOutlet UITextField *passwordField;
@property (nonatomic, assign) IBOutlet UISwitch *marketingEmailSwitch;
@property (nonatomic, assign) IBOutlet UISwitch *notificationSwitch;
@property (copy, nonatomic) void (^submitHandler) (NSDictionary*properties);
@property (copy, nonatomic) NSArray *(^validationHandler) (NSDictionary*properties);

@property (nonatomic, retain) id delegate;

-(IBAction)toggleLoginOrNew:(id)sender;
-(IBAction)submitLoginOrNew:(id)sender;

@end
