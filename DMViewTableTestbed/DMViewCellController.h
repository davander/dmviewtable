//
//  DMExampleOneController.h
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-08.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMViewTable.h"

@interface DMViewCellController : NSObject <DMViewTableCellController>

@property (nonatomic, retain) UIView *view;
@property (nonatomic, retain) NSString *title;
@property (readwrite) BOOL expanded;

-(void)viewDragged:(UIScrollView *)scrollView;

@end
