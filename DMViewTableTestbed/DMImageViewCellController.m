//
//  DMImageViewCellController.m
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-14.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMImageViewCellController.h"

@implementation DMImageViewCellController

@synthesize imageView;

-(BOOL)expanded {
    return YES;
}

@end
