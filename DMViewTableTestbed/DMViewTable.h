//
//  DMViewTable.h
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-08.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DMViewTableCellController <NSObject>

-(BOOL)expanded;
-(CGFloat)height; // or -(CGFloat)expandedHeight; -(CGFloat)collapsedHeight;

-(void)expandCell:(BOOL)animated;
-(void)collapseCell:(BOOL)animated;

@property (nonatomic, retain) UIView *view;

@end


@interface DMViewTable : UITableView

@end
