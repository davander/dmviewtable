//
//  DMExampleOneController.m
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-08.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMViewCellController.h"
#import <QuartzCore/QuartzCore.h>

@implementation DMViewCellController

@synthesize expanded;
@synthesize view;
@synthesize title;

-(CGFloat)height {
    return (self.expanded ? self.view.bounds.size.height : 44);
}

-(void)expandCell:(BOOL)animated {
    self.expanded = YES;
}

-(void)collapseCell:(BOOL)animated {
    self.expanded = NO;
}


#pragma mark -
#pragma Lifecycle

- (id)init {
    self = [super init];
    if (self) {
        // Custom initialization
        NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
        NSAssert(nibContents && [nibContents count], @"Could not load xib. Expected one called %@",NSStringFromClass([self class]));
        for (id currentObject in nibContents) {
            if ([currentObject isKindOfClass:[UIView class]]) {
                self.view = currentObject;
                self.view.clipsToBounds = YES;
                self.view.layer.cornerRadius = 10.0f;
                self.view.layer.masksToBounds = YES;
                break;
            }
        }
    }
    return self;
}

-(void)dealloc {
    self.view = nil;
    self.title = nil;
    [super dealloc];
}

-(void)viewDragged:(UIScrollView *)scrollView {
    //nothing here, override to do stuff.
}

@end
