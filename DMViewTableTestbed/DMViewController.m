//
//  DMViewController.m
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-08.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import "DMViewController.h"
#import "DMViewTableController.h"
#import "DMViewCellController.h"
#import "DMImageViewCellController.h"
#import "DMAccountCellController.h"

@implementation DMViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:10];
    
    
    DMViewCellController *vc1 = [[[DMImageViewCellController alloc] init] autorelease];
    DMViewCellController *vc2 = [[[DMImageViewCellController alloc] init] autorelease];
    DMViewCellController *vc3 = [[[DMAccountCellController alloc] init] autorelease];
    DMViewCellController *vc4 = [[[DMViewCellController alloc] init] autorelease];
    
    vc1.title = NSStringFromClass([vc1 class]);
    vc2.title = NSStringFromClass([vc2 class]);
    vc3.title = NSStringFromClass([vc3 class]);
    vc4.title = NSStringFromClass([vc4 class]);
    
    [array addObject:vc1];
    [array addObject:vc2];
    [array addObject:vc3];
    [array addObject:vc4];

    
    DMViewTableController *viewTableController = [[DMViewTableController alloc] init];
    viewTableController.grouped = NO;
    viewTableController.viewControllers = array;
    viewTableController.viewTable.tableFooterView = [[[UIView alloc] initWithFrame:(CGRect){.size.height = 200, .size.width = 320}] autorelease];
    [self presentModalViewController:[viewTableController autorelease] animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
