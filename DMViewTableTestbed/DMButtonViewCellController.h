//
//  DMCancelViewCellController.h
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-03-13.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMViewCellController.h"

@interface DMButtonViewCellController : DMViewCellController

@property (nonatomic, retain) IBOutlet UIButton *button;
@property (nonatomic, copy) void (^buttonHandler) (void);

-(id)initWithTitle:(NSString *)buttonTitle;
-(IBAction)buttonPress:(id)sender;

@end
