//
//  DMAppDelegate.h
//  DMViewTableTestbed
//
//  Created by Cory Alder on 12-02-08.
//  Copyright (c) 2012 Davander Mobile Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DMViewController;

@interface DMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) DMViewController *viewController;

@end
